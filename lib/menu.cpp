#include "menu.h"
OBJ *menu;
OBJ *menuGameOver;
void onDisplayMenu(bool &menuShow,bool &gameOver)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glm::mat4 V = glm::mat4(1.f);

	if(gameOver)drawObj(menuGameOver,V);
	else drawObj(menu,V);
}
