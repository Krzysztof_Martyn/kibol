#include "main.h"

#ifndef POSTPROCESS_H
#define POSTPROCESS_H

class Postprocess
{
	private:
	GLuint fbo, fbo_texture, rbo_depth;
	GLuint vbo_fbo_vertices;
	GLuint program_postproc, attribute_v_coord_postproc, uniform_fbo_texture,uniform_offset;
	ShaderProgram *shaderPostProc;
	int windowWidth, windowHeight;
	public:
	int initPostProgram(const char* fFilename);
	void freePostVertices();
	int initPostProces();
	void freePostProces();
	void initPostVertices();
	void updateMove(float speedWave);
	void changeSize(int windowWidth,int windowHeight);
	void onDisplayBegin();
	void onDisplayEnd();
	Postprocess(){};
};


#endif
