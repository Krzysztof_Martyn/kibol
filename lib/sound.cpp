#include "sound.h"
ALfloat listenerPos[MAX_SOUND];//={0.0,0.0,4.0};
ALfloat listenerVel[MAX_SOUND];//={0.0,0.0,0.0};
ALfloat	listenerOri[MAX_SOUND];//={0.0,0.0,1.0, 0.0,1.0,0.0};


ALfloat sourcePos[MAX_SOUND][3];//={ -2.0, 0.0, 0.0};
ALfloat sourceVel[MAX_SOUND][3];//={ 0.0, 0.0, 0.0};




ALuint	buffer[NUM_BUFFERS];
ALuint	source[NUM_SOURCES];
ALuint  environment[NUM_ENVIRONMENTS];


ALsizei size,freq;
ALenum 	format;
ALvoid 	*data;
ALboolean albool;
int 	ch;
void startNewSound(int i, int &sound_nr)
{
	if(sound_nr ==-1)
		{	
			sound_nr=i;
			soundStart(sound_nr);
		}
	else if (sound_nr!=i)
		{
			soundStop(sound_nr);
			sound_nr=i;
			soundStart(sound_nr);
		}
}
void stopSound(int &sound_nr)
{
	
		soundStop(sound_nr);
		sound_nr = -1;
}
void soundStart(int i)
{
	alSourcePlay(source[i]);
}
void soundStop(int i)
{
	alSourceStop(source[i]);
}
void soundEnd()
{
	for(int i=0;i<MAX_SOUND;i++)
		alSourceStop(source[i]);
	alutExit();
}
void addSound(int i, const char * name)
{
	alutLoadWAVFile((ALbyte*)name,&format,&data,&size,&freq, &albool);
    alBufferData(buffer[i],format,data,size,freq);
    alutUnloadWAV(format,data,size,freq);
}
void bindSound(ALboolean looping ,int i)
{
	    alSourcef(source[i],AL_PITCH,1.0f);
   		alSourcef(source[i],AL_GAIN,1.0f);
    	alSourcefv(source[i],AL_POSITION,sourcePos[i]);
    	alSourcefv(source[i],AL_VELOCITY,sourceVel[i]);
    	alSourcei(source[i],AL_BUFFER,buffer[i]);
    	alSourcei(source[i],AL_LOOPING,looping);
}
void initSound(void)
{
    alutInit(0, NULL);
	for(int i=0;i<MAX_SOUND;i++)
	{
		listenerPos[i] = 0.0f;
		listenerVel[i] = 0.0f;
		listenerOri[i] = 0.0f;
		for(int j=0;j<3;j++)
		{
			sourcePos[i][j] = 0.0f;
			sourceVel[i][j] = 0.0f;
		}	
	}
    alListenerfv(AL_POSITION,listenerPos);
    alListenerfv(AL_VELOCITY,listenerVel);
    alListenerfv(AL_ORIENTATION,listenerOri);
    
    alGetError(); // clear any error messages
    
    if(alGetError() != AL_NO_ERROR) 
    {
        printf("- Error creating buffers !!\n");
        exit(1);
    }
    else
    {
        printf("init() - No errors yet.\n");

    }
    printf("alGenBuffers ...\n");
    // Generate buffers, or else no sound will happen!
    alGenBuffers(NUM_BUFFERS, buffer);
    printf("alGenBuffers ok\n");
	addSound(0,"./sound/ekranS1.wav");
	addSound(1,"./sound/ekranS2.wav");
	addSound(2,"./sound/1.wav");
	addSound(3,"./sound/thunder.wav");
	addSound(4,"./sound/bonusBialy.wav");
	addSound(5,"./sound/bonusCzarny.wav");
	addSound(6,"./sound/dieColor.wav");
	addSound(7,"./sound/diePink.wav");
	addSound(8,"./sound/naskok.wav");
	addSound(9,"./sound/podpalenie.wav");
	addSound(10,"./sound/podpalenieRainbow.wav");
	addSound(11,"./sound/podpalenieSklot.wav");
	addSound(12,"./sound/podpalenieTrash.wav");
	addSound(13,"./sound/podpalenieZnicz.wav");
	addSound(14,"./sound/sign.wav");
	addSound(15,"./sound/zranienie.wav");
	addSound(16,"./end/kibol_koniec_1.wav");
	addSound(17,"./end/kibol_koniec_2.wav");
    alGetError(); /* clear error */
    alGenSources(NUM_SOURCES, source);

    if(alGetError() != AL_NO_ERROR) 
    {
        printf("- Error creating sources !!\n");
        exit(2);
    }
    else
    {
        printf("init - no errors after alGenSources\n");
    }

	for(int i =0;i<3;i++)
	{
		bindSound(AL_TRUE,i);
	}
		for(int i =3;i<19;i++)
	{
		bindSound(AL_FALSE,i);
	}
}
