LIBS=-lGL -lglut -lGLEW -lalut -lopenal
INCPATH  	= -I../include -I/usr/local/include -I/usr/X11R6/include -I/usr/include -I/usr/include/freetype2 
ELSE= -fno-strict-aliasing  -L/usr/X11R6/lib  -lGL -lGLU -lX11 -lXext  -lpthread -lm -lGLEW -lfreetype  -L/usr/local/lib  -lm -ldl -g  -MMD -MP
CC=g++ -std=c++11
LIB_DIR =./lib/
SOURCES=$(LIB_DIR)main_file.cpp $(LIB_DIR)tga.cpp $(LIB_DIR)shaderprogram.cpp $(LIB_DIR)OBJ.cpp $(LIB_DIR)player.cpp $(LIB_DIR)mouse.cpp $(LIB_DIR)shader_utils.cpp $(LIB_DIR)text.cpp $(LIB_DIR)display.cpp $(LIB_DIR)gra.cpp $(LIB_DIR)postprocess.cpp $(LIB_DIR)enemy.cpp $(LIB_DIR)sound.cpp 
HEADERS=#tga.h  shaderprogram.h OBJ.h player.h mouse.h common/shader_utils.cpp text.h
OBJECTS=$(SOURCES:.cpp=.o)

all: main_file

main_file: $(OBJECTS) 
	$(CC) -o ./run $(OBJECTS) $(LIBS) $(ELSE)  $(INCPATH) -w 

$(OBJECTS): %.o: %.cpp $(HEADERS)
	$(CC) -c $< -o $@ -w $(INCPATH)
	
clean:
	-rm -rf $(LIB_DIR)*.o run
