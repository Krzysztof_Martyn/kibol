#include "display.h"

void assignVBOtoAttribute(OBJ *o,char* attributeName, GLuint *bufVBO, int variableSize) {
	GLuint location=o->shaderProgram->getAttribLocation(attributeName); //Pobierz numery slotów dla atrybutu
	glBindBuffer(GL_ARRAY_BUFFER,*bufVBO);  //Uaktywnij uchwyt VBO 
	glEnableVertexAttribArray(location); //W³¹cz u¿ywanie atrybutu o numerze slotu zapisanym w zmiennej location
	glVertexAttribPointer(location,variableSize,GL_FLOAT, GL_FALSE, 0, NULL); //Dane do slotu location maj¹ byæ brane z aktywnego VBO
}
void drawObjectEnd(OBJ *o,int texId)
{
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D,*(o->getTex(texId)));
	//Uaktywnienie VAO i tym samym uaktywnienie predefiniowanych w tym VAO powi¹zañ slotów atrybutów z tablicami z danymi
	glBindVertexArray((o->vao));
	assignVBOtoAttribute(o,const_cast<char*> ("texCoords"),o->bufTexCoords,2); 
	assignVBOtoAttribute(o,const_cast<char*> ("vertex"),o->bufVertices,4);
	assignVBOtoAttribute(o,const_cast<char*> ("normal"),o->bufNormals,4);
	//Narysowanie obiektu
	glDrawArrays(GL_TRIANGLES,0,o->vertexCount);
	
	//Posprz¹tanie po sobie (niekonieczne w sumie je¿eli korzystamy z VAO dla ka¿dego rysowanego obiektu)
	glBindVertexArray(0);
}
void drawObjPrototype(OBJ *o,glm::mat4 &matV,glm::mat4 matP, int texId)
{
	o->shaderProgram->use();
	

	glUniformMatrix4fv(o->shaderProgram->getUniformLocation("P"),1, false, glm::value_ptr(matP));
	glUniformMatrix4fv(o->shaderProgram->getUniformLocation("V"),1, false, glm::value_ptr(matV));//cout<<"2"<<endl;
	glUniformMatrix4fv(o->shaderProgram->getUniformLocation("M"),1, false,  glm::value_ptr(o->matM));
	
	drawObjectEnd(o,texId);
}
void drawObj(OBJ *o,glm::mat4 &matV) {
	drawObjPrototype(o,matV,glm::mat4(1.f),o->texId);

}

void drawObjEnemy(OBJ *o,glm::mat4 &matV ,int i){

	glm::mat4 matP=glm::translate(glm::mat4(1.0f),glm::vec3(0.f, 0.f, 0.f-(i+1)*0.000001f));
	drawObjPrototype(o,matV,matP,o->texId);
}
void drawObjectPlayer(OBJ *o,Player *p, int i,glm::mat4 &matV) {

	o->shaderProgram->use();
	glm::mat4 matP=glm::translate(glm::mat4(1.0f),glm::vec3(0.f, 0.f, 0.f-(i+1)*0.01f));

	
	glUniformMatrix4fv(o->shaderProgram->getUniformLocation("P"),1, false, glm::value_ptr(matP));
	glUniformMatrix4fv(o->shaderProgram->getUniformLocation("V"),1, false, glm::value_ptr(matV));//cout<<"2"<<endl;
	glUniformMatrix4fv(o->shaderProgram->getUniformLocation("M"),1, false,  glm::value_ptr(o->matM));
	glUniformMatrix4fv(o->shaderProgram->getUniformLocation("M2"),1, false,  glm::value_ptr(o->matM2));
	glUniformMatrix4fv(o->shaderProgram->getUniformLocation("MR"),1, false,  glm::value_ptr(p->matR));
	glUniformMatrix4fv(o->shaderProgram->getUniformLocation("MS"),1, false,  glm::value_ptr(p->matSize));
	glUniform1f(o->shaderProgram->getUniformLocation("a"),p->a);
	
	drawObjectEnd(o,0);
}
void calculateMatVP(Matrix &matrix , float &tabScreen, int &ekran)
{
	//Wylicz macierz rzutowania
	matrix.P=glm::mat4(1.0f);//glm::perspective(cameraAngle, (float)windowWidth/(float)windowHeight, 1.0f, 100.0f);
	if(tabScreen<0)
		{
			matrix.V = glm::translate(matrix.V0  ,glm::vec3(0.f, tabScreen*4.5, 0.f));	
			ekran = round(-tabScreen/0.4441);
			//cout<<tabScreen<<" "<<ekran<<endl;
		}
	else 
	{
		tabScreen=0;		
		matrix.V= matrix.V0;
	}
}

void calculateBackgroundFrame(int nr, int max, BackgroundFrame &bFrame)
{
	nr = mod(nr,max);
	bFrame.frame[0] = nr-2;
	bFrame.frame[1] = nr-1;
	bFrame.frame[2] = nr;
	bFrame.frame[3] = nr+1;
	bFrame.frame[4] = nr+2;
	if(nr==0)
	{
		bFrame.frame[0]=max-2;
		bFrame.frame[1]=max-1;
	}
	else if (nr==1)
	{
		bFrame.frame[0]=max-1;
	}
	else if(nr==max-2)
	{
		bFrame.frame[4] = 0;
	}
	else if(nr==max-1)
	{
		bFrame.frame[4] = 1;
		bFrame.frame[3] = 0;
	}
}
void calculateAmmo(vector<OBJ> &ammo)
{
	for(int i=0;i<ammo.size();i++)
	{
		int left = ammo[i].lookAtLeft ? -1: 1;
		left+=0;
		float poprawka=-1+ 4*ammo[i].blend/AMMO_SPEED;
			//cout<<poprawka<<endl;
		ammo[i].matM = glm::translate(ammo[i].matM2  ,glm::vec3(0.f, left*ammo[i].blend/AMMO_SPEED*PINK_RANGE, 0.f));	
		int k =  ammo[i].x2+round(left*(ammo[i].blend/AMMO_SPEED*PINK_RANGE-poprawka));
		if( abs(k-ammo[i].x) <2)
			ammo[i].x = k;
	}
	
}
void calculateHpLevel(OBJ & hp)
{
	hp.matM   =  glm::scale(hp.matM2, glm::vec3(1.f, hp.blend+0.000001f, 0.f));
	hp.matM   =  glm::translate(hp.matM   ,glm::vec3(0.f, -1/(hp.blend+0.000001f)+1, 0.f));	
}
string strAppendInt(string str, int i)
{
	string tmp; // brzydkie rozwiązanie
	sprintf((char*)tmp.c_str(), "%d", i);
	string str2 = tmp.c_str();
	str.append(str2);
	return str;
}
void DrawText(Text *text)
{
	float sx = 2.0 / glutGet(GLUT_WINDOW_WIDTH);
	float sy = 2.0 / glutGet(GLUT_WINDOW_HEIGHT);
	text->initDisplayText(80);
	text->render_text("POZIOM PATRIOTYZMU", 0.25+ 8 * sx, 1-30 * sy, sx * 0.5, sy * 0.5);
	//text->render_text("PLAYER 2", 1 - 250 * sx, 1-30 * sy, sx * 0.5, sy * 0.5);
	/*text->initDisplayText(65);
	text->render_text("LIVES", -1 + 8 * sx, 1-75 * sy, sx * 0.5, sy * 0.5);
	text->render_text("LIVES", 1 - 250 * sx, 1-75 * sy, sx * 0.5, sy * 0.5);
	text->render_text((const char*)(strAppendInt("BEERS ",players[0]->beers).c_str()), -1 + 8 * sx, 1-120 * sy, sx * 0.5, sy * 0.5);
	text->render_text((const char*)(strAppendInt("BEERS ",players[1]->beers).c_str()), 1 - 250 * sx, 1-120 * sy, sx * 0.5, sy * 0.5);
	text->render_text((const char*)(strAppendInt("SHOTS ",players[0]->shots).c_str()), -1 + 8 * sx, 1-165 * sy, sx * 0.5, sy * 0.5);
	text->render_text((const char*)(strAppendInt("SHOTS ",players[1]->shots).c_str()), 1 - 250 * sx, 1-165 * sy, sx * 0.5, sy * 0.5);
*/}

