#include "player.h"

Player::Player(char * name,int id)
{
	
	initPad(name);
	this->id =id;
}
int Player::initPad(char * name)  
{
		
        if( ( joy_fd = open( name , O_RDONLY)) == -1 )
        {
                printf( "Couldn't open joystick\n" );
                return -1;
        }

        ioctl( joy_fd, JSIOCGAXES, &num_of_axis );
        ioctl( joy_fd, JSIOCGBUTTONS, &num_of_buttons );
        ioctl( joy_fd, JSIOCGNAME(80), &name_of_joystick );

        axis = (int *) calloc( num_of_axis, sizeof( int ) );
        button = (char *) calloc( num_of_buttons, sizeof( char ) );

        printf("Joystick detected: %s\n\t%d axis\n\t%d buttons\n\n"
                , name_of_joystick
                , num_of_axis
                , num_of_buttons );
}

int Player::pad( int &x, int &y, int &b,bool menuShow) 
{
	fcntl( joy_fd, F_SETFL, O_NONBLOCK ); 
                        /* read the joystick state */
	read(joy_fd, &js, sizeof(struct js_event));
              /* see what to do with the event */
	switch (js.type & ~JS_EVENT_INIT)
	{
		case JS_EVENT_AXIS:
		axis   [ js.number ] = js.value;
		break;
		case JS_EVENT_BUTTON:
		button [ js.number ] = js.value;
		break;
	}
	flag =5; 
		
	if(/*(a<MAP_X-1)and*/(!menuShow)&&(b*axis[5]>0))//>  
	{
		flag =0;


	}
	else if((!menuShow)&&(a>0)&&(b*axis[5]<0))//<
	{
	
		flag =1;
	}
	 else if((!menuShow)&&(b*axis[6]<0))//^
	{ 
	
		flag =4;
		
	}
	else if(button[9])return 9;
	else if(button[8])return 8;
	return flag;
}
