#include "main.h"


float speedWave=1.0f;

//Macierze

float tabScreen=0.f;
int ekran =0;
BackgroundFrame backgroundFrame;

//Ustawienia okna i rzutowania
int windowPositionX=100;
int windowPositionY=100;
int windowWidth=1280;
int windowHeight=720;
float cameraAngle=45.0f;

//Zmienne do animacji
float speed=12; //120 stopni/s
int lastTime=0;
float angle=0;
float swapT=0.0f;
int b=1;
vector<vector<OBJ>> obiekty;
vector<OBJ> postacie;
vector<OBJ> background;
vector<vector<OBJ>> enemy;
vector<OBJ> prototypeList;
vector<OBJ> ammo;
vector<OBJ> hpObject;
vector<vector<OBJ>> bonusList;
Text *text;
int enemyCount=300;

Player **players;
OBJ *o; // obiekt główny


Gra kibol;
Matrix matrix;
Postprocess postprocess;

int countEnemyColor=0;
//Procedura rysuj¹ca jaki obiekt. Ustawia odpowiednie parametry dla vertex shadera i rysuje.



bool menuShow = true;
float menuShowStart2=false;
bool menuShowStart = false;
bool gameOver = false;
bool win = false;
OBJ *menu;
OBJ *menuGameOver;
OBJ *menuWin;
OBJ *menuStart;
OBJ *endAnim;

int soundNr=-1;
int soundEffect=-1;
float winTime=0.0f;
float winStage=0;
int timeWait =0;
//Procedura rysuj¹ca



void displayFrame() {
 	startNewSound(2,soundNr);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	
	for(int i=1;i<4;i++)
	{
		int frame = backgroundFrame.frame[i];
		OBJ *o = &(background[frame]); 
		o->matM =  glm::translate(glm::mat4(1.0f) , glm::vec3(0.f, (ekran+i-2)*2.f, 0.1f));//środek 256
		drawObj(o,matrix.V);
	}
	DrawText(text);
		for(int i=0;i<ammo.size();i++)
	{
		drawObjEnemy(&(ammo[i]),matrix.V,-i-enemyCount);
	}
	for(int i=0;i<5;i++)
	{
		int frame = backgroundFrame.frame[i];
		
		for(int j=0;j<obiekty[frame].size();j++)
		{
			drawObjEnemy(&(obiekty[frame][j]),matrix.V,enemyCount+frame*16);
		}
		for(int j=0;j<enemy[frame].size();j++)
		{
			if(enemy[frame][j].visible)drawObjEnemy(&(enemy[frame][j]),matrix.V,j+16*frame);
		}
		for(int j=0;j<bonusList[frame].size();j++)
		{
			drawObjEnemy(&(bonusList[frame][j]),matrix.V,j+16*frame);
		}
	}

	//cout<<enemy.size();
	
	
	
	for(int i=0;i<2;i++)
	{
		
		drawObjectPlayer(&(postacie[i]),(players[i]),i,matrix.V);
	}
	
	

}
void onDisplayMenu(bool &menuShow,bool &gameOver, bool &win)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glm::mat4 V = glm::mat4(1.f);
	V = glm::rotate(V  ,-90.f,glm::vec3(0.f,0.f,1.f));
	V = glm::rotate(V  ,180.f,glm::vec3(1.f,0.f,0.f));
	if(menuShowStart) {	
	drawObj(menuStart,V);
	startNewSound(1,soundNr);
	}
	else if(gameOver)drawObj(menuGameOver,V);
	else if (win)drawObj(endAnim,V);
	else {
	drawObj(menu,V);
	startNewSound(0,soundNr);
	}
}
void onDisplay() {
 // logic();
		
	calculateMatVP(matrix,tabScreen,ekran);
	calculateBackgroundFrame(ekran, BACKGOUND_NUMBER, backgroundFrame);
	calculateAmmo(ammo);
	calculateHpLevel(hpObject[1]);
	postprocess.onDisplayBegin();

	if(menuShow)onDisplayMenu(menuShow,gameOver,win);
	else displayFrame();

	postprocess.onDisplayEnd();
	if(!menuShow)
	{
		
		for(int i=0;i<hpObject.size();i++)
		{
			glm::mat4 v = glm::mat4(1.0f);
			v  = glm::translate(v,glm::vec3(1.275f, 0.f, 0.f));
			drawObjEnemy(&(hpObject[i]),v,i);
		}
	}
  	glutSwapBuffers();
}




//Procedura wywo³ywana przy zmianie rozmiaru okna
void changeSize(int w, int h) {
	//Ustawienie wymiarow przestrzeni okna
	glViewport(0,0,w,h);
	//Zapamiêtanie nowych wymiarów okna dla poprawnego wyliczania macierzy rzutowania
	windowWidth=w;
	windowHeight=h;
	postprocess.changeSize(w,h);


}
int mod(int i,int j)
{
	return i%j;
}
float ABS(float i)
{
	if(i<0) return -i;
	else return i;
}
void freeVector(vector<OBJ> &vec)
{
	while(vec.size())
	{
		vec.pop_back();
	}
}
void bogosort()
{
	for(int i=0;i<20;i++)
	{
		int r = rand()%(BACKGOUND_NUMBER -1) +1;
		int r2 = rand()%(BACKGOUND_NUMBER -1) +1;
		GLuint ** c1 = background[r].texture;
	
		background[r].texture = background[r2].texture;
		background[r2].texture = c1;
	}
}
void nextLevel()
{
	
	bogosort();
	for(int i=0;i< obiekty.size();i++)freeVector(obiekty[i]);
	for(int i=0;i< enemy.size();i++)freeVector(enemy[i]);
	for(int i=0;i< bonusList.size();i++)freeVector(bonusList[i]);
	freeVector(ammo);
	int desc = 0;
	int enemyPos = rand()%23+1;
	countEnemyColor=0;
	addSklot(enemy,matrix,&(prototypeList[6]),30*enemyPos, 0);//1
	desc += 1<<enemyPos;
	cout<<endl<<enemyPos<< " "<<desc<<endl;

	for(int i=0;i<2;i++)
	{
		while(1<<enemyPos & desc)enemyPos = rand()%23+1;
		addSklot(enemy,matrix,&(prototypeList[6]),30*enemyPos, 0);//1
		desc += 1<<enemyPos;
		cout<<endl<<enemyPos<< " "<<desc<<endl;
	}
	for(int o=0;o<3;o++)
	{
		while(1<<enemyPos & desc)enemyPos = rand()%23+1;
	
		desc += 1<<enemyPos;
		cout<<enemyPos<< " "<<desc<<endl;
		addRainbow(enemy,matrix,&(prototypeList[7]),30*enemyPos, 0);//2
	}

	for(int o =0;o<8;o++)
	{
		while(1<<enemyPos & desc)enemyPos = rand()%23+1;
		desc+=1<<enemyPos;
		cout<<enemyPos<< " "<<desc<<endl;
		int licznosc = 2+rand()%3;
		for(int i=0;i<licznosc;i++)
		{
			addPink(enemy,matrix,&(prototypeList[1]),30*enemyPos-13 + i*4, 0);//3
		}	
	}
	

	while(1<<enemyPos & desc)enemyPos = rand()%23+1;
	desc+=1<<enemyPos;
	cout<<enemyPos<< " "<<desc<<endl;
	for(int i=0;i<2;i++)
	{
		addColor(enemy,matrix,&(prototypeList[5]),30*enemyPos-13 + i*5, 0);//5
	}	

	while(1<<enemyPos & desc)enemyPos = rand()%23+1;
	desc+=1<<enemyPos;
	cout<<enemyPos<< " "<<desc<<endl;
	for(int i=0;i<2;i++)
	{
		addColor(enemy,matrix,&(prototypeList[5]),30*enemyPos-13 + i*5, 0);//6
	}
	while(1<<enemyPos & desc)enemyPos = rand()%23+1;
	desc+=1<<enemyPos;
	cout<<enemyPos<< " "<<desc<<endl;
	for(int i=0;i<2;i++)
	{
		addColor(enemy,matrix,&(prototypeList[5]),30*enemyPos-13 + i*5, 0);//6
	}		
	for(int i =0;i<4;i++)
	{
		while(1<<enemyPos & desc)enemyPos = rand()%23+1;
		desc+=1<<enemyPos;
		cout<<enemyPos<< " "<<desc<<endl;
		int position = rand()%18;
		addTrash(enemy,matrix,&(prototypeList[8]),30*enemyPos-14 +position, 0);//7
		if(rand()%3==0)
		addTrash(enemy,matrix,&(prototypeList[8]),30*enemyPos-14 +position+2, 0);//7
	}	

		while(1<<enemyPos & desc)enemyPos = rand()%23+1;
	desc+=1<<enemyPos;
	cout<<enemyPos<< " "<<desc<<endl;
	addThomb(enemy,matrix,&(prototypeList[12]),30*enemyPos-14 +rand()%20, 0);//11	
	
		while(1<<enemyPos & desc)enemyPos = rand()%23+1;
	desc+=1<<enemyPos;
	cout<<enemyPos<< " "<<desc<<endl;
	addThomb(enemy,matrix,&(prototypeList[12]),30*enemyPos-14 +rand()%20, 0);//12
	
	for(int i=0;i<40;i++)
	{
		addThunder(bonusList,matrix,&(prototypeList[11]),rand()%600+30, -1);
	}
	
	int dist=20;
	int high=2;
	for(int i=0;i<500&& dist<650;i++)
	{
		int rx= rand()%9+1;
		int ry= rand()%5-2;
		high+=ry;
		if(high<3)high =3;
		else if(high>11) high = 11;

		for(int j=0;j<rx;j++)
		{
			dist++;
			if(rand()%4)addBrick(obiekty,matrix,&(prototypeList[10]),dist, high);
			else if(high<10)addBlock(obiekty,matrix,&(prototypeList[9]),dist, high);
			if(rand()%17==0)addThunder(bonusList,matrix,&(prototypeList[11]),dist+rand()%5-2, 13+rand()%3);
		}
		dist+=rand()%6;		
			
	}	
	//for(int i=0;i<30;i++)addBrick(obiekty,matrix,&(prototypeList[10]),16+i+30, 4);	
	//addPink(enemy,matrix,&(prototypeList[1]),10, 0);
	//addColor(enemy,matrix,&(prototypeList[5]),17, 0);
	//countEnemyColor ++;
	//addSklot(enemy,matrix,&(prototypeList[6]),60, 0);
	//addRainbow(enemy,matrix,&(prototypeList[7]),30, 0);
	//addTrash(enemy,matrix,&(prototypeList[8]),5, 0);

		int k=0;
	for(int i=0;i<enemy.size();i++)
	{
		k+=enemy[i].size();
		
	}
	enemyCount = k;
}
int moveRight(int i, float m)
{
	players[i]->a+=MOVE_A*m;
		if(ABS(players[0]->a-players[1]->a)*SPACE_MAX<SPACE_A)
		{
			
			if((players[0]->a >5.5-0.000823026) &&(players[1]->a>5.5))
			{
				players[0]->a-=5.5-0.000823026;
				players[1]->a-=5.5-0.000823026;
				tabScreen+= 11-2*0.000823026;
				nextLevel();
				
			}
			tabScreen-=MOVE_A*m;
			//cout<<"p"<<i<<" = "<<players[i]->a<<endl;
			//cout<<"tabScreen = "<<-tabScreen*4.74<<endl;
			return 1;
		}
		else
		{
			players[i]->a-=MOVE_A*m;
			return 0;
		}
}
int moveLeft(int i, float m)
{
		players[i]->a-=MOVE_A*m;
		if(ABS(players[0]->a-players[1]->a)*SPACE_MAX<SPACE_A)
		{
			
			//cout<<"p"<<i<<" = "<<players[i]->a<<endl;
			tabScreen+=MOVE_A*m;
			return 1;
		}
		else
		{
			players[i]->a+=MOVE_A*m;
			return 0;
		}	
}
void chckPlayer(int i)
{
	collisionX(i);
	//cout<<players[i]->a<<endl;
	switch(players[i]->pM )
	{
	case 5:
		//cout<<endl;
		postacie[i].anim =0;
		players[i]->go = false;
		//if(obiekty.size()==1)nextLevel();
		break;	
	
	case 0://>
		//players[i]->a +=0.0005f;
		if(moveRight(i,1))
		{
			players[i]->matR =glm::rotate( glm::mat4(1.0f) ,180.f,glm::vec3(1.f,0.f,0.f));
			postacie[i].anim =1;		
			postacie[i].x = round(players[i]->a*SPACE_MAX*1.5f);
			if(postacie[i].x<16) postacie[i].ekran=0;
			else postacie[i].ekran = round((postacie[i].x-16)/30)+1;
			players[i]->go = true;
			//cout<<i<<" "<<postacie[i].ekran<<endl;
			//cout<<"postac"<<i<<" = "<<postacie[i].x<<endl;
		}
		break;
	case 1://<
		
		if(moveLeft(i,1))
		{
			players[i]->matR = glm::mat4(1.0f);
			postacie[i].anim =1;
			postacie[i].x = round(players[i]->a*SPACE_MAX*1.5f);
			if(postacie[i].x<16) postacie[i].ekran=0;
			else postacie[i].ekran = round((postacie[i].x-16)/30)+1;
			players[i]->go = true;
			//cout<<i<<" "<<postacie[i].ekran<<endl;
			//cout<<"postac"<<i<<" = "<<postacie[i].x<<endl;
		}
		break;

	case 4:  
		//cout<<"skok4"<<endl;
		if(!players[i]->jump_up)
		{
			//players[i]->jump = 1.0f;//max 1.6f
			players[i]->jump_up = true;
		}
		break;	
	}
	
	//cout<<i<< " "<<postacie[i].x<<" "<<postacie[i].y<<endl;
}
int jumpUp(int i,float speed_inferval)
{
	if((players[i]->jump<1.0f+players[i]->floor) and(players[i]->jump<1.55f*3/players[i]->high))
		{	
			postacie[i].matM2 = glm::translate(postacie[i].matM2 ,glm::vec3(-speed_inferval, 0.f, 0.f));
			players[i]->jump+=speed_inferval;
			return 0;
		}
		else
		{
			return 1;
		}
}
int jumpDown(int i,float speed_inferval)
{
		if(players[i]->jump>0.f)
		{	
			postacie[i].matM2 = glm::translate(postacie[i].matM2 ,glm::vec3(speed_inferval, 0.f, 0.f));
			players[i]->jump-=speed_inferval;
			return 0;
		}
		else
		{
			postacie[i].matM2 = glm::translate(postacie[i].matM2 ,glm::vec3(players[i]->jump, 0.f, 0.f));
			players[i]->jump=0;
			return 1;
		}
}
int collisionTop(int j)
{
	srand(time(NULL));
	int ekran = postacie[j].ekran;
	for(int i=0; i<obiekty[ekran].size();i++)
	{
		
		if((obiekty[ekran][i].x == postacie[j].x)and (obiekty[ekran][i].y == 	players[j]->top))
		{
			if((obiekty[ekran][i].anim==0)&&(obiekty[ekran][i].type==0))
			{
				soundStart(14);
				obiekty[ekran][i].anim=1;
				obiekty[ekran][i].texId = 1;
				 int r =rand()%5;
				if(r==0 )addBlack(bonusList,matrix,&(prototypeList[3]),obiekty[ekran][i].x, obiekty[ekran][i].y+1);

				else if(r==1 || r ==2 ) 
					{
						addWhite(bonusList,matrix,&(prototypeList[4]),obiekty[ekran][i].x, obiekty[ekran][i].y+1);
					}		
				else addThunder(bonusList,matrix,&(prototypeList[11]),obiekty[ekran][i].x, obiekty[ekran][i].y+1);
				cout<<1<<r<<endl;
			}
			else if(obiekty[ekran][i].type==1)
			{
				obiekty[ekran][i]= obiekty[ekran][obiekty[ekran].size()-1];

				obiekty[ekran].pop_back();
			}
			//obiekty[i].matM = glm::translate(obiekty[i].matM  ,glm::vec3(-0.04f, 0.f, 0.f));
			//cout<<"kolizja "<<j<<" "<<i<<endl;
			return 1;
		}
		
	}
	return 0;
}
int collisionBottom(int j)
{
	int ekran = postacie[j].ekran;
	//cout<<enemy[ekran].size()<<endl;
	for(int i=0; i<enemy[ekran].size();i++)
	{
		if((enemy[ekran][i].visible)and(enemy[ekran][i].type<3)and(enemy[ekran][i].x == postacie[j].x)and (enemy[ekran][i].y+1 == 	players[j]->bottom))
		{
			/*if(enemy[ekran][i].type ==1) 	countEnemyColor--;		
			enemy[ekran][i]= enemy[ekran][enemy.size()-1];
			enemy[ekran].pop_back();
			enemyCount--;
			i--;
			if(i<0)i=0;*/
			soundStart(8);
			enemy[ekran][i].visible= false;
			cout<<"gin "<<i<<endl;
			if(hpObject[1].blend<1)hpObject[1].blend+= MAX_POINT* 0.01f;
				else 
				{
					menuShow = true;
					win = true;
				}
		}
	}
	
	for(int i=0; i<obiekty[ekran].size();i++)
	{
		if((obiekty[ekran][i].x == postacie[j].x)and (obiekty[ekran][i].y == 	players[j]->bottom))
		{
			
			//obiekty[i].matM = glm::translate(obiekty[i].matM  ,glm::vec3(-0.04f, 0.f, 0.f));
			if(obiekty[ekran][i].bonus==0)
			{
				//cout<<"stoje "<<j<<" "<<i<<endl;
				return 1;
			}
			
		}
	}
	
	return 0;
}
int collisionAmmo( OBJ & postac)
{
	for(int i =0;i< ammo.size();i++)
	{
		//cout<<"a"<<i<<": "<<ammo[i].x<<" p: "<<postac.x<<endl;
		if (ammo[i].x == postac.x)
		{
			if(hpObject[1].blend>0)hpObject[1].blend-= 0.03f;
			ammo[i]= ammo[ammo.size()-1];
			ammo.pop_back();
			soundStart(15);
			return 1;
		}
	} 
	return 0;
}
int collisionX(int j)
{
	int ekran = postacie[j].ekran;
		for(int i=0; i<bonusList[ ekran].size();i++)
	{
		if((bonusList[ ekran][i].x == postacie[j].x)and (bonusList[ ekran][i].y == 	players[j]->bottom+1))
		{
			
			//obiekty[i].matM = glm::translate(obiekty[i].matM  ,glm::vec3(-0.04f, 0.f, 0.f));
			if(bonusList[ ekran][i].bonus==0)
			{
				//cout<<"stoje "<<j<<" "<<i<<endl;
				return 1;
			}
			else if((bonusList[ ekran][i].bonus==1)&&(players[j]->little))
			{
				//cout<<"player1 "<<j<<endl;
					//cout<<obiekty[i].bonus<<endl;
					soundStart(5);
					players[j]->matSize  = glm::mat4(1.f);
					if(bonusList[ekran].size()>1)
						bonusList[ekran][i]= bonusList[ekran][bonusList[ekran].size()-1];
					players[j]->little= false;
					bonusList[ekran].pop_back();
					if(hpObject[1].blend<1)hpObject[1].blend+= MAX_POINT* 0.01f;
					else 
					{
						menuShow = true;
						win = true;
					}
				//cout<<"player1_ "<<j<<endl;
			}
			else if((bonusList[ ekran][i].bonus==2) && (!players[j]->tourch))
			{
				//cout<<"player2 "<<j<<endl;
				soundStart(4);
				postacie[j].swapBankTex();
				players[j]->tourch = true;
				if(bonusList[ekran].size()>1)
					bonusList[ ekran][i]= bonusList[ ekran][bonusList[ ekran].size()-1];

				bonusList[ ekran].pop_back();
				if(hpObject[1].blend<1)hpObject[1].blend+= MAX_POINT* 0.01f;
				else 
				{
					menuShow = true;
					win = true;
				}
				//cout<<"player2_ "<<j<<endl;
			}
			else if(bonusList[ ekran][i].bonus==3)
			{
				//cout<<"playere "<<j<<endl;
				soundStart(3);
				if(hpObject[1].blend<1) hpObject[1].blend+= MAX_POINT* 0.01f;
				else 
				{
					menuShow = true;
					win = true;
				}
				if(bonusList[ekran].size()>1)
					bonusList[ ekran][i]= bonusList[ ekran][bonusList[ ekran].size()-1];

				bonusList[ ekran].pop_back();
				//cout<<"playere_ "<<j<<endl;
			}
		}
	}
	return 0;
}
void skoki(int i)
{
	if((players[i]->jump_down) or ((players[i]->floor)and(!players[i]->jump_up)))
	{
		if(collisionBottom(i))
		{
			players[i]->jump_down = false;
			players[i]->jump_up = false;
			players[i]->floor = players[i]->jump;
		}
		else if(jumpDown(i,0.002f))
		{
			players[i]->jump_down = false;
			players[i]->jump_up = false;
			players[i]->floor =0.f;
			postacie[i].matM2 = matrix.M2_begin[i];

		}
		players[i]->bottom = round(players[i]->jump*10*(players[i]->high*0.33333f)-1.5);
		players[i]->top = players[i]->bottom+ players[i]->high;
		//cout<<"postac"<<i<<" = "<<postacie[i].y<<endl;
	}
	else if(players[i]->jump_up)
	{		
		if(jumpUp(i,0.002f)or (collisionTop(i))) 
		{			
			players[i]->jump_down = true;
			players[i]->jump_up = false;
			//players[i]->jump=1.0f;
		}
		
		players[i]->bottom = round(players[i]->jump*10*(players[i]->high*0.33333f)-1.5);
		players[i]->top = players[i]->bottom+ players[i]->high;
		//cout<<"postac"<<i<<" = "<<postacie[i].y<<endl;
	}
	
}
void calcMat2(int i,int interval)
{
	float speed_inferval = speed*interval/10.0;
	int st = players[i]->pM;
	if (postacie[i].anim==0)
	{
		
		int k=players[i]->pad(postacie[i].x,postacie[i].y, b,menuShow);
		if(k==8)menuShow =true;
		else
		{
			players[i]->pM =k;
			chckPlayer(i);
			postacie[i].matM= glm::translate(matrix.M_begin ,glm::vec3(0.f, players[i]->a*SPACE_MAX, 0.f));
		}
	}

	else if ((postacie[i].anim==1)and(postacie[i].blend<1) )
	{
	 postacie[i].blend+=speed_inferval;
	}
	else if (postacie[i].anim==1)
	{
		postacie[i].blend=0;
		postacie[i].anim=0;
		//postacie[i].matM = postacie[i].matM2;
	}
	skoki(i);

	//cout<<i<<" "<<postacie[i].anim<<" "<<postacie[i].blend<<" "<<players[i]->pM<<endl;
}
void soundEffectSwitch(OBJ &enemy)
{
	switch(enemy.type)
	{
		case 0 :
		soundStart(9);//pink
		break;
		case 1 :
		soundStart(9);//color
		break;
		case 7 :
		soundStart(11);//sklot
		break;
		case 3 :
		soundStart(10);//rainbow
		break;
		case 4 :
		soundStart(12);//trash
		break;
		case 8 :
		soundStart(13);//thomb
		break;
	}
}
int  arson(int enemyId,int nr,int ekran)//podpalenie
{
	if(nr ==100)
			{
				for(int i=0;i<postacie.size();i++)
				{
					
					if((enemy[ekran][enemyId].y == players[i]->bottom+2)&&(players[i]->tourch))
					{
						soundEffectSwitch(enemy[ekran][enemyId]);
						players[i]->tourch = false;
						postacie[i].swapBankTex();
						if(hpObject[1].blend<1)hpObject[1].blend+= MAX_POINT* 0.01f;
						else 
						{
							menuShow = true;
							win = true;
						}
						return 1;
					}
				}
			}
	else 
	{
		
		if((enemy[ekran][enemyId].y == players[nr]->bottom+2)&&(players[nr]->tourch))
		{
			//cout<<enemyId<<" "<<nr<<" "<<enemy[enemyId].y<<"\t"<<players[nr]->bottom+1<<endl;
			soundEffectSwitch(enemy[ekran][enemyId]);
			players[nr]->tourch = false;
			postacie[nr].swapBankTex();
			if(hpObject[1].blend<1)hpObject[1].blend+= MAX_POINT* 0.01f;
				else 
				{
					menuShow = true;
					win = true;
				}						
			return 1;
		}
	}
	return 0;
}
void enemyLogic(int interval)
{
	for(int ekran=0;ekran<enemy.size();ekran++)
	{
		int enemySize = enemy[ekran].size();
		for(int i=0;i<enemySize;i++)
		{
			if(enemy[ekran][i].visible)
			{
				
				int dist_min;
				int nr;
				int flag = enemyDistance(enemy[ekran][i],postacie,dist_min,PINK_RANGE,nr);
			
				if((flag)&&(abs(dist_min)<2)&&(nr>-1)&&(!enemy[ekran][i].bonus))
				{
				
					enemy[ekran][i].bonus = arson(i,nr,ekran);//podpalenie
					
				}
		
				if(enemy[ekran][i].texId ||enemy[ekran][i].anim)
				{	
					enemy[ekran][i].blend+=interval;
					if(enemy[ekran][i].blend>1000)
					{
						enemy[ekran][i].blend=0.0;
						if(enemy[ekran][i].type==0)
						{
							switch (enemy[ekran][i].bonus)
							{
								case 0: enemy[ekran][i].anim = enemy[ekran][i].nextTexId(0,2);
								break;
								case 1: enemy[ekran][i].anim = enemy[ekran][i].nextTexId(3,4);
								enemy[ekran][i].bonus = 2;
								break;
								case 2: enemy[ekran][i].anim = enemy[ekran][i].nextTexId(5,6);
								break;
							}
							if(enemy[ekran][i].anim ==2 )
								addAmmo(ammo,matrix,&(prototypeList[2]),enemy[ekran][i].x, enemy[ekran][i].y,enemy[ekran][i].lookAtLeft);
							
						}
						else
						{
							switch (enemy[ekran][i].bonus)
							{
								case 0: enemy[ekran][i].anim = 0;//enemy[i].nextTexId(0,2);
								break;
								case 1: enemy[ekran][i].anim = enemy[ekran][i].nextTexId(1,4);
								enemy[ekran][i].bonus = 2;
								break;
								case 2: enemy[ekran][i].anim = enemy[ekran][i].nextTexId(5,6);
								break;
							}
						}
						if ((countEnemyColor<MAX_ENEMY_COLOR )&&(flag)&&(enemy[ekran][i].type == 1)) 
						{
							int r = rand()%2*2-1;
							addColor(enemy,matrix,&(prototypeList[5]),enemy[ekran][i].x+3*r, enemy[ekran][i].y);
							enemy[ekran][i].type = 2;
							countEnemyColor ++;
							enemyCount++;
							cout<<r<<" dodaje kolorowego "<<enemy[ekran].size()<<" "<<enemy[ekran][enemy[ekran].size()-1].y<<endl;
						}
					}
					
				}
				else
				{
			
					enemy[ekran][i].anim = flag;	
					enemyLookAt(enemy[ekran][i],dist_min);
				}
			}
		}
	}
}
void ammoLogic(int interval)
{
	for(int i=0;i<ammo.size();i++)
	{
		ammo[i].blend+=interval;
		if(ammo[i].blend>AMMO_SPEED)
		{
			ammo[i]= ammo[ammo.size()-1];
			//cout<<"ammo end "<<i<<endl;
			ammo.pop_back();
			
		}
	}
}

//Procedura uruchamiana okresowo. Robi animacjê.
void nextFrame(void) {
	int actTime=glutGet(GLUT_ELAPSED_TIME);
	int interval=actTime-lastTime;
	lastTime=actTime;
	if(menuShowStart2>0)
		menuShowStart2 -=interval;
	if(menuShow)
	{
		if(win &&timeWait ==0)
		{ 
			soundStop(soundNr);
			if(winStage==0 &&winTime ==0.0f)
				soundStart(16);
			if(winStage==12 &&winTime ==0.0f)
				soundStart(17);
			winTime+=interval;
			if(winTime >500.0f)//milisekundy
			{
				winTime=0.0f;
				if(winStage<12)
				{
					endAnim->texId = mod(endAnim->texId+1,2);
				}
				else if(winStage<12+4)
					endAnim->texId = 2;
				else endAnim->texId = endAnim->nextTexId();
				if(winStage<50+12+4-4)winStage++;
				else timeWait++;
			
			}
		}
		else if (timeWait>0)
		{
			winTime+=interval;
			if(winTime >2000.0f) exit(0);
		}
		for(int i=0;i<2;i++)
		{
			int b;
			int k =players[i]->pad(postacie[i].x,postacie[i].y, b, menuShow);
			if(k==9) 
			{
				if(menuShowStart && menuShowStart2<=0)
				{
					menuShow = false;
					win = false;
					gameOver = false;
					menuShowStart = false;
					menuShowStart2 = -1;
				}
				else
				{
					
					menuShowStart = true;
					menuShowStart2 = 100;
				}
			}
				
		}
	}
	else
	{
		enemyLogic( interval);
		
		if((players[0]->bottom == -2)||(players[0]->bottom == -1))
		{
			if(collisionAmmo(postacie[0])) 
				{
					players[0]->matSize = glm::scale(glm::mat4(1.f), glm::vec3(1.f, 1.f/2, 0.f));
					players[0]->little= true;
				}
		}
		ammoLogic(interval);
		swapT+=interval;

		if(swapT>150)
		{ 
		
		
			swapT =0;
			for(int i=0;i<2;i++)
			{
			
				if(((players[i]->go || players[i]->go_stand) 
					&& !players[i]->jump_up && !players[i]->jump_down)
					||(players[i]->jump_up && players[i]->jump_down && !players[i]->go_stand))
				{
					postacie[i].swapTex();
					players[i]->go_stand = !players[i]->go_stand;
				}
			}
		
		}
		 postprocess.updateMove( speedWave);
	
		for(int i=0;i<2;i++)
		{//cout<<"1"<<endl;
			if(players[i]->lives>0)calcMat2(i,interval);
		}
	}
	glutPostRedisplay();
}

bool loadMap(char *name)
{		
	std::ifstream plik;
	plik.open(name);
	for(int i =MAP_Y-1;i>-1;i--)
	{ //cout<<endl;
		for(int j=0;j<MAP_X;j++) 
		{
			if( !plik.good() )
         	return false;
			
		}
	} //cout<<endl;
	return true;
}
void addPlayer(OBJ *o,string texName,string texName2,string texName3,string texName4)
{
	OBJ *o2= new OBJ(4);
	o2->readTexture(texName,0);
	o2->readTexture(texName2,1);
	o2->readTexture(texName3,2);
	o2->readTexture(texName4,3);
	o2->setupOBJ(o);
	o2->setupShaders(VSHADER_PLAYER,NULL,FSHADER_PLAYER);
	postacie.push_back(*o2);
}


void addPlayerMap(int nr)
{


	players[nr]->a= nr*MOVE_A;
	postacie[nr].x = 0;
	postacie[nr].y = 0;
	postacie[nr].ekran=0;
	postacie[nr].matM =matrix.M_begin;
	postacie[nr].matM2 = matrix.M2_begin[nr];
	//postacie[nr].matM2=M2;
	
}
void initObiectMap()
{
	loadMap(MAPA);	
	
	
}
void loadBackground(OBJ *o1,string filename,int i)
{
	OBJ *o = new OBJ(1);
	o->setupOBJ(o1);
	o->readTexture(filename,0);
	o->setupShaders(o1);
	o->matM =  glm::translate(glm::mat4(1.0f) , glm::vec3(0.f, i*2.f, 0.1f));//środek 256
	background.push_back(*o);
}

void addAllPrototype()
{
	OBJ *prototype = addPinkPrototype(&(prototypeList[0]));//1
	prototypeList.push_back(*prototype);
	OBJ *prototype2 = addAmmoPrototype(&(prototypeList[0]));//2
	prototypeList.push_back(*prototype2);
	OBJ *prototype3 = addBlackPrototype(&(prototypeList[0]));//3
	prototypeList.push_back(*prototype3);
	OBJ *prototype4 = addWhitePrototype(&(prototypeList[0]));//4
	prototypeList.push_back(*prototype4);
	OBJ *prototype5 = addColorPrototype(&(prototypeList[0]));//5
	prototypeList.push_back(*prototype5);
	OBJ *prototype6 = addSklotPrototype(&(prototypeList[0]));//6
	prototypeList.push_back(*prototype6);
	OBJ *prototype7 = addRainbowPrototype(&(prototypeList[0]));//7
	prototypeList.push_back(*prototype7);
	OBJ *prototype8 = addTrashPrototype(&(prototypeList[0]));//8
	prototypeList.push_back(*prototype8);
	OBJ *prototype9 = addBlockPrototype(&(prototypeList[0]));//9
	prototypeList.push_back(*prototype9);
	OBJ *prototype10 = addBrickPrototype(&(prototypeList[0]));//10
	prototypeList.push_back(*prototype10);
	OBJ *prototype11 = addThunderPrototype(&(prototypeList[0]));//11
	prototypeList.push_back(*prototype11);
	OBJ *prototype12 = addThombPrototype(&(prototypeList[0]));//11
	prototypeList.push_back(*prototype12);
}
void addHp(OBJ *o1,string filename, float level)
{
	OBJ *o = new OBJ(1);
	o->setupOBJ(o1);
	o->readTexture(filename,0);
	o->setupShaders(o1);
	o->matM =  glm::translate(matrix.M_begin , glm::vec3(-15*0.66666f, -9.9*0.66666f, 0.f));//środek 256
	o->matM =  glm::scale(o->matM, glm::vec3(0.33333f, 3.3333f, 0.f));
	o->blend = level;
	o->matM = matrix.V0* o->matM;
	o->matM2 = o->matM;
	hpObject.push_back(*o);
}
void addMenu(OBJ *o1,string filename,OBJ *o)
{
	
	o->setupOBJ(o1);
	o->readTexture(filename,0);
	o->setupShaders(o1);
	o->matM = glm::mat4(1.f);
}
void addEnd(OBJ *o1,OBJ *o)
{
	o->setupOBJ(o1);
	o->setupShaders(o1);
	o->matM = glm::mat4(1.f);
	o->readTexture("./end/kibol - victory 1.tga",0);
	o->readTexture("./end/kibol - victory 2.tga",1);
	o->readTexture("./end/kibol - win 8 - kibole.tga",2);
	o->readTexture("./end/kibol - win 9.tga",3);
	o->readTexture("./end/kibol - win 10.tga",4);
	o->readTexture("./end/kibol - win 11.tga",5);
	o->readTexture("./end/kibol - win 12.tga",6);
	o->readTexture("./end/kibol - win 13.tga",7);
	o->readTexture("./end/kibol - win 14.tga",8);
	o->readTexture("./end/kibol - win 15.tga",9);
	o->readTexture("./end/kibol - win 16.tga",10);
	o->readTexture("./end/kibol - win 17.tga",11);
	o->readTexture("./end/kibol - win 18.tga",12);
	o->readTexture("./end/kibol - win 19.tga",13);
	o->readTexture("./end/kibol - win 20.tga",14);
	o->readTexture("./end/kibol - win 21.tga",15);
	o->readTexture("./end/kibol - win 22.tga",16);
	o->readTexture("./end/kibol - win 23.tga",17);
	o->readTexture("./end/kibol - win 24.tga",18);
	o->readTexture("./end/kibol - win 25.tga",19);
	o->readTexture("./end/kibol - win 26.tga",20);
	o->readTexture("./end/kibol - win 27.tga",21);
	o->readTexture("./end/kibol - win 28.tga",22);
	o->readTexture("./end/kibol - win 29.tga",23);
	o->readTexture("./end/kibol - win 30.tga",24);
	o->readTexture("./end/kibol - win 31.tga",25);
	o->readTexture("./end/kibol - win 32.tga",26);
	o->readTexture("./end/kibol - win 33.tga",27);
	o->readTexture("./end/kibol - win 34.tga",28);
	o->readTexture("./end/kibol - win 35.tga",29);
	o->readTexture("./end/kibol - win 36.tga",30);
	o->readTexture("./end/kibol - win 37.tga",31);
	o->readTexture("./end/kibol - win 38.tga",32);
	o->readTexture("./end/kibol - win 39.tga",33);
	o->readTexture("./end/kibol - win 40.tga",34);
	o->readTexture("./end/kibol - win 41.tga",35);
	o->readTexture("./end/kibol - win 42.tga",36);
	o->readTexture("./end/kibol - win 43.tga",37);
	o->readTexture("./end/kibol - win 44.tga",38);
	o->readTexture("./end/kibol - win 45.tga",39);
	o->readTexture("./end/kibol - win 46.tga",40);
	o->readTexture("./end/kibol - win 47.tga",41);
	o->readTexture("./end/kibol - win 48.tga",42);
	o->readTexture("./end/kibol - win 49.tga",43);
	o->readTexture("./end/kibol - win 50.tga",44);
	o->readTexture("./end/kibol - win 51.tga",45);
	o->readTexture("./end/kibol - win 52.tga",46);
	o->readTexture("./end/kibol - win 53.tga",47);
	o->readTexture("./end/kibol - win 54.tga",48);
	o->readTexture("./end/kibol - win 55.tga",49);
	
}
void loadObject()
{
	obiekty.resize(BACKGOUND_NUMBER+1);
	enemy.resize(BACKGOUND_NUMBER+1);
	bonusList.resize(BACKGOUND_NUMBER+1);
	o= new OBJ(PLANE);
	o->setupShaders(VSHADER_MAIN,NULL,FSHADER_MAIN);

	menu = new OBJ(1);
	addMenu(o,"./texture/menu.tga",menu);
	menuGameOver = new OBJ(1);
	addMenu(o,"./texture/menuGameOver.tga",menuGameOver);
	menuWin = new OBJ(1);
	addMenu(o,"./texture/menuWin.tga",menuWin);
	menuStart = new OBJ(1);
	addMenu(o,"./texture/menuStart.tga",menuStart);
	endAnim  = new OBJ(50);
	addEnd(o,endAnim);
	prototypeList.push_back(*o);
	int i =0;
	string backgroundName;
	for(int i=0;i<BACKGOUND_NUMBER;i++)
	{
	
		 backgroundName= (string)BACKGROUND_PATH + "Tlo_" + std::to_string(i)+".tga";
	
		loadBackground(o,backgroundName,i);
	}
	cout<<"loadBackground"<<endl;
	
	addAllPrototype();
	cout<<"addPrototype"<<endl;
	addPink(enemy,matrix,&(prototypeList[1]),10, 0);
	//addColor(enemy,matrix,&(prototypeList[5]),17, 0);
	//countEnemyColor ++;
	addSklot(enemy,matrix,&(prototypeList[6]),60, 0);
	addRainbow(enemy,matrix,&(prototypeList[7]),30, 0);
	addTrash(enemy,matrix,&(prototypeList[8]),5, 0);
	addThunder(bonusList,matrix,&(prototypeList[11]),5, 7);
	addHp(o,"./texture/obiect/hp0.tga", 1.f);
	addHp(o,"./texture/obiect/hp1.tga", 0.f);	
	
	addPlayer(o,"./texture/hero/kibol0_0.tga","./texture/hero/kibol0_1.tga","./texture/hero/kibol0_2.tga","./texture/hero/kibol0_3.tga");
	addPlayer(o,"./texture/hero/kibol1_0.tga","./texture/hero/kibol1_1.tga","./texture/hero/kibol1_2.tga","./texture/hero/kibol1_3.tga");


	addPlayerMap(0);
	addPlayerMap(1);

	cout<<"player add"<<endl;

	
	
	for(int i=0;i<300;i++)addBlock(obiekty,matrix,&(prototypeList[9]),i, 4);	
	for(int i=0;i<300;i++)addBrick(obiekty,matrix,&(prototypeList[10]),i+300, 4);	

	nextLevel();
}

//Procedura inicjuj¹ca biblotekê glut
void initGLUT(int *argc, char** argv) {
	glutInit(argc,argv); //Zainicjuj bibliotekê GLUT
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH); //Alokuj bufory kolorów (podwójne buforowanie) i bufor kolorów
	
	glutInitWindowPosition(windowPositionX,windowPositionY); //Wska¿ pocz¹tkow¹ pozycjê okna
	glutInitWindowSize(windowWidth,windowHeight); //Wska¿ pocz¹tkowy rozmiar okna
	glutCreateWindow("OpenGL 3.3"); //Utwórz okno i nadaj mu tytu³
//	glutIdleFunc(pad);
	glutReshapeFunc(changeSize); //Zarejestruj procedurê changeSize jako procedurê obs³uguj¹ca zmianê rozmiaru okna
	glutDisplayFunc(onDisplay); //Zarejestruj procedurê displayFrame jako procedurê obs³uguj¹ca odwierzanie okna
	glutIdleFunc(nextFrame); //Zarejestruj procedurê nextFrame jako procedurê wywo³ywan¹ najczêciêj jak siê da (animacja)
	glutFullScreen();  
}


//Procedura inicjuj¹ca bibliotekê glew
void initGLEW() {
	GLenum err=glewInit();
	if (GLEW_OK!=err) {
		fprintf(stderr,"%s\n",glewGetErrorString(err));
		exit(1);
	}
}
 
void free_all()
{
	postprocess.freePostProces();
	postprocess.freePostVertices();
	delete(text);
}
void init_matrix()
{
	matrix.V0=glm::rotate(glm::mat4(1.0f),-90.0f,glm::vec3(0,0,1));

	matrix.M_begin = glm::scale(glm::mat4(1.f), glm::vec3(0.15f, 0.1f, 0.f));
	matrix.M_begin = glm::translate(matrix.M_begin,glm::vec3(4.5f, 0.f, 0.f));
	matrix.M_begin = glm::translate(matrix.M_begin,glm::vec3(0.0f, 0.f, 0.f));
	
	//matrix.M_end = glm::translate(matrix.M_begin,glm::vec3(0.f, SPACE_MAX+spaceY, 0.f));
	matrix.M2_begin[0] = glm::mat4(1.f);
	matrix.M2_begin[1] = glm::translate(glm::mat4(1.f),glm::vec3(0.275f, 0.f, 0.f));
	matrix.M2_begin[1] = glm::scale(matrix.M2_begin[1], glm::vec3(0.66666f, 1.f, 0.f));
	players[1]->matSize = glm::scale(glm::mat4(1.f), glm::vec3(1.f, 1.f/2, 0.f));
	players[0]->matSize = glm::scale(glm::mat4(1.f), glm::vec3(1.f, 1.f/2, 0.f));
	players[0]->bottom =-2;
	players[1]->bottom =-2;
	players[0]->top =3;
	players[1]->top =2;
	players[0]->high =3;
	players[1]->high =2;
	players[0]->little =true;
	players[1]->little =true;
}
int main(int argc, char** argv) {
	
	srand(time(NULL));
	initGLUT(&argc,argv);
	initGLEW();
	glEnable(GL_DEPTH_TEST);//cout<<"1"<<endl;
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA) ;
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	cout<<"init opengl"<<endl;
	players= new Player*[2];
	players[0] = new Player(JOY_DEV0,-2);
	players[1] = new Player(JOY_DEV1,-3);
	init_matrix();cout<<"matrix load"<<endl;
	loadObject();cout<<"oblect load"<<endl;

	
	//players = new Player(JOY_DEV);
	postprocess.initPostProces();cout<<"ipp"<<endl;
	postprocess.initPostVertices();cout<<"ipv"<<endl;
	postprocess.initPostProgram(FSHADER_POST);cout<<"ipp"<<endl;
	text = new Text();
	text->setFont(FONT_NAME);
	text->init_resources();
	cout<<"sound...";
	initSound();
	cout<<"OK"<<endl;
	glutMainLoop();
	  //close( joy_fd ); 
	free_all();
	soundEnd();
	return 0;
}
