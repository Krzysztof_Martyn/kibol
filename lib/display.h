
#include "main.h"
#ifndef DISPLAY_H
#define DISPLAY_H
typedef struct BackgroundFrame{
int frame[5];
};
void assignVBOtoAttribute(OBJ *o,char* attributeName, GLuint *bufVBO, int variableSize) ;
void drawObj(OBJ *o,glm::mat4 &matV);
void drawObjEnemy(OBJ *o,glm::mat4 &matV ,int i);
void drawObjectPlayer(OBJ *o,Player *p, int i,glm::mat4 &matV) ;
void calculateMatVP(Matrix &matrix , float &tabScreen, int &ekran);
void DrawText(Text *text);
void drawObjectEnd(OBJ *o,int texId);
void drawObjPrototype(OBJ *o,glm::mat4 &matV,glm::mat4 matP, int texId);
void calculateBackgroundFrame(int nr, int max, BackgroundFrame &frame);
void calculateAmmo(vector<OBJ> &ammo);
void calculateHpLevel(OBJ & hp);
string strAppendInt(string str, int i);
#endif
