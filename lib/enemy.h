#include "main.h"
#ifndef ENEMY_H
#define ENEMY_H
OBJ * addPinkPrototype(OBJ *o1);
OBJ * addAmmoPrototype(OBJ *o1);
OBJ * addBlackPrototype(OBJ *o1);
OBJ * addWhitePrototype(OBJ *o1);
OBJ * addColorPrototype(OBJ *o1);
OBJ * addSklotPrototype(OBJ *o1);
OBJ * addRainbowPrototype(OBJ *o1);
OBJ * addTrashPrototype(OBJ *o1);
OBJ * addBlockPrototype(OBJ *o1);
OBJ * addBrickPrototype(OBJ *o1);
OBJ * addThunderPrototype(OBJ *o1);
OBJ * addThombPrototype(OBJ *o1);
void addAmmo(vector<OBJ> &ammo,Matrix matrix,OBJ *prototype,int x, int y,bool side);

void addPink(vector<vector<OBJ>> &enemy,Matrix matrix,OBJ *prototype,int x, int y);
void addBlack(vector<vector<OBJ>> &enemy,Matrix matrix,OBJ *prototype,int x, int y);
void addWhite(vector<vector<OBJ>> &enemy,Matrix matrix,OBJ *prototype,int x, int y);
void addColor(vector<vector<OBJ>> &enemy,Matrix matrix,OBJ *prototype,int x, int y);
void addSklot(vector<vector<OBJ>> &enemy,Matrix matrix,OBJ *prototype,int x, int y);
void addRainbow(vector<vector<OBJ>> &enemy,Matrix matrix,OBJ *prototype,int x, int y);
void addTrash(vector<vector<OBJ>> &enemy,Matrix matrix,OBJ *prototype,int x, int y);
void addThomb(vector<vector<OBJ>>  &enemy,Matrix matrix,OBJ *prototype,int x, int y);

void addBlock(vector<vector<OBJ>> &enemy,Matrix matrix,OBJ *prototype,int x, int y);
void addBrick(vector<vector<OBJ>> &enemy,Matrix matrix,OBJ *prototype,int x, int y);

void addThunder(vector<vector<OBJ>> &enemy,Matrix matrix,OBJ *prototype,int x, int y);
void enemyLookAt(OBJ &o,int dist_min);
int enemyDistance(OBJ &o,vector<OBJ> postacie,int &dist_min,int distance,int &nr);
#endif
