#include "enemy.h"
OBJ * addPinkPrototype(OBJ *o1)//sprawdzić
{
	OBJ *o2 = new OBJ();
	o2->setupShaders(VSHADER_MAIN,NULL,FSHADER_MAIN);
	
	OBJ *o = new OBJ(7);
	o->setupOBJ(o1);
	o->readTexture("./texture/enemy/pink0.tga",0);
	o->readTexture("./texture/enemy/pink1.tga",1);
	o->readTexture("./texture/enemy/pink2.tga",2);
	o->readTexture("./texture/enemy/pink3.tga",3);
	o->readTexture("./texture/enemy/pink4.tga",4);
	o->readTexture("./texture/enemy/pink5.tga",5);
	o->readTexture("./texture/enemy/pink6.tga",6);
	o->setupShaders(o2);
	
	o->bonus = 0;
	
	delete(o2);
	return o;
}
OBJ * addAmmoPrototype(OBJ *o1)
{
		OBJ *o2 = new OBJ();
	o2->setupShaders(VSHADER_MAIN,NULL,FSHADER_MAIN);
	
	OBJ *o = new OBJ(1);
	o->setupOBJ(o1);
	o->readTexture("./texture/obiect/ammo.tga",0);
	o->setupShaders(o2);
	
	
	
	delete(o2);
	return o;
}
OBJ * addBlackPrototype(OBJ *o1)
{
	//loadObject(o,"./texture/enemy/black.tga","./texture/enemy/black.tga",obiekty[i].x,obiekty[i].y+1,1);
	OBJ *o2 = new OBJ();
	o2->setupShaders(VSHADER_MAIN,NULL,FSHADER_MAIN);
	OBJ *o = new OBJ(1);
	o->setupOBJ(o1);
	o->readTexture("./texture/enemy/black.tga",0);
	//o->readTexture("./texture/enemy/black.tga",1);
	o->setupShaders(o2);
	
	
	delete(o2);
	return o;

}
OBJ * addWhitePrototype(OBJ *o1)
{
	//loadObject(o,"./texture/enemy/black.tga","./texture/enemy/black.tga",obiekty[i].x,obiekty[i].y+1,1);
	OBJ *o2 = new OBJ();
	o2->setupShaders(VSHADER_MAIN,NULL,FSHADER_MAIN);
	OBJ *o = new OBJ(1);
	o->setupOBJ(o1);
	o->readTexture("./texture/enemy/bialy.tga",0);
	//o->readTexture("./texture/enemy/black.tga",1);
	o->setupShaders(o2);
	
	
	delete(o2);
	return o;

}
OBJ * addColorPrototype(OBJ *o1)//sprawdzić
{
	OBJ *o2 = new OBJ();
	o2->setupShaders(VSHADER_MAIN,NULL,FSHADER_MAIN);
	
	OBJ *o = new OBJ(7);
	o->setupOBJ(o1);
	o->readTexture("./texture/enemy/color0.tga",0);
	o->readTexture("./texture/enemy/color1.tga",1);
	o->readTexture("./texture/enemy/color2.tga",2);
	o->readTexture("./texture/enemy/color3.tga",3);
	o->readTexture("./texture/enemy/color4.tga",4);
	o->readTexture("./texture/enemy/color5.tga",5);
	o->readTexture("./texture/enemy/color6.tga",6);
	o->setupShaders(o2);
	
	o->bonus = 0;
	
	delete(o2);
	return o;
}
OBJ * addSklotPrototype(OBJ *o1)//sprawdzić
{
	OBJ *o2 = new OBJ();
	o2->setupShaders(VSHADER_MAIN,NULL,FSHADER_MAIN);
	
	OBJ *o = new OBJ(7);
	o->setupOBJ(o1);
	o->readTexture("./texture/enemy/sklot0.tga",0);
	o->readTexture("./texture/enemy/sklot1.tga",1);
	o->readTexture("./texture/enemy/sklot2.tga",2);
	o->readTexture("./texture/enemy/sklot3.tga",3);
	o->readTexture("./texture/enemy/sklot4.tga",4);
	o->readTexture("./texture/enemy/sklot5.tga",5);
	o->readTexture("./texture/enemy/sklot6.tga",6);
	o->setupShaders(o2);
	
	o->bonus = 0;
	
	delete(o2);
	return o;
}
OBJ * addRainbowPrototype(OBJ *o1)//sprawdzić
{
	OBJ *o2 = new OBJ();
	o2->setupShaders(VSHADER_MAIN,NULL,FSHADER_MAIN);
	
	OBJ *o = new OBJ(7);
	o->setupOBJ(o1);
	o->readTexture("./texture/enemy/rainbow0.tga",0);
	o->readTexture("./texture/enemy/rainbow1.tga",1);
	o->readTexture("./texture/enemy/rainbow2.tga",2);
	o->readTexture("./texture/enemy/rainbow3.tga",3);
	o->readTexture("./texture/enemy/rainbow4.tga",4);
	o->readTexture("./texture/enemy/rainbow5.tga",5);
	o->readTexture("./texture/enemy/rainbow6.tga",6);
	o->setupShaders(o2);
	
	o->bonus = 0;
	
	delete(o2);
	return o;
}
OBJ * addTrashPrototype(OBJ *o1)
{
	OBJ *o2 = new OBJ();
	o2->setupShaders(VSHADER_MAIN,NULL,FSHADER_MAIN);
	
	OBJ *o = new OBJ(7);
	o->setupOBJ(o1);
	o->readTexture("./texture/enemy/trash0.tga",0);
	o->readTexture("./texture/enemy/trash1.tga",1);
	o->readTexture("./texture/enemy/trash2.tga",2);
	o->readTexture("./texture/enemy/trash3.tga",3);
	o->readTexture("./texture/enemy/trash4.tga",4);
	o->readTexture("./texture/enemy/trash5.tga",5);
	o->readTexture("./texture/enemy/trash6.tga",6);
	o->setupShaders(o2);
	
	o->bonus = 0;
	
	delete(o2);
	return o;
}
OBJ * addThombPrototype(OBJ *o1)
{
	OBJ *o2 = new OBJ();
	o2->setupShaders(VSHADER_MAIN,NULL,FSHADER_MAIN);
	
	OBJ *o = new OBJ(7);
	o->setupOBJ(o1);
	o->readTexture("./texture/enemy/pomnik0.tga",0);
	o->readTexture("./texture/enemy/pomnik1.tga",1);
	o->readTexture("./texture/enemy/pomnik2.tga",2);
	o->readTexture("./texture/enemy/pomnik3.tga",3);
	o->readTexture("./texture/enemy/pomnik4.tga",4);
	o->readTexture("./texture/enemy/pomnik5.tga",5);
	o->readTexture("./texture/enemy/pomnik6.tga",6);
	o->setupShaders(o2);
	
	o->bonus = 0;
	
	delete(o2);
	return o;
}
OBJ * addBlockPrototype(OBJ *o1)
{
	OBJ *o2 = new OBJ();
	o2->setupShaders(VSHADER_MAIN,NULL,FSHADER_MAIN);
	OBJ *o = new OBJ(2);
	o->setupOBJ(o1);
	o->readTexture("./texture/obiect/blok_0.tga",1);
	o->readTexture("./texture/obiect/blok_1.tga",0);
	o->setupShaders(o2);
	
	o->bonus = 0;
	
	delete(o2);
	return o;
}
OBJ * addBrickPrototype(OBJ *o1)
{
	OBJ *o2 = new OBJ();
	o2->setupShaders(VSHADER_MAIN,NULL,FSHADER_MAIN);
	
	OBJ *o = new OBJ(1);
	o->setupOBJ(o1);
	o->readTexture("./texture/obiect/brick.tga",0);

	o->setupShaders(o2);
	
	o->bonus = 0;
	
	delete(o2);
	return o;
}
OBJ * addThunderPrototype(OBJ *o1)
{
	OBJ *o2 = new OBJ();
	o2->setupShaders(VSHADER_MAIN,NULL,FSHADER_MAIN);
	
	OBJ *o = new OBJ(1);
	o->setupOBJ(o1);
	o->readTexture("./texture/obiect/thunder.tga",0);

	o->setupShaders(o2);
	
	o->bonus = 0;
	
	delete(o2);
	return o;
}
void addWhite(vector<vector<OBJ>>  &enemy,Matrix matrix,OBJ *prototype,int x, int y)
{
	OBJ *o = new OBJ();
	o->setupOBJ(prototype);
	o->setupTexAll(prototype);
	o->matM =  glm::translate(matrix.M_begin , glm::vec3(-y*0.66666f - 0.33333, x*0.66666f, 0.f));//środek 256
	o->matM =  glm::scale(o->matM, glm::vec3(0.66666f, 0.33333f, 0.f));
	
	o->x =x;
	o->y =y;
	if(x<16) o->ekran=0;
	else o->ekran = round((x-16)/30)+1;
	o->bonus = 2;
	enemy[o->ekran].push_back(*o);
	//cout<<"dodano"<<endl;
}
void addBlack(vector<vector<OBJ>>  &enemy,Matrix matrix,OBJ *prototype,int x, int y)
{
	OBJ *o = new OBJ();
	o->setupOBJ(prototype);
	o->setupTexAll(prototype);
	o->matM =  glm::translate(matrix.M_begin , glm::vec3(-y*0.66666f, x*0.66666f, 0.f));//środek 256
	o->matM =  glm::scale(o->matM, glm::vec3(0.33333f, 0.33333f, 0.f));
	o->x =x;
	o->y =y;
	if(x<16) o->ekran=0;
	else o->ekran = round((x-16)/30)+1;
	o->bonus = 1;
	enemy[o->ekran].push_back(*o);
	//cout<<"dodano"<<endl;
}
void addPink(vector<vector<OBJ>>  &enemy,Matrix matrix,OBJ *prototype,int x, int y)
{
	
	
	OBJ *o = new OBJ();
	o->setupOBJ(prototype);
	o->setupTexAll(prototype);
	o->matM =  glm::translate(matrix.M_begin , glm::vec3(-y*0.66666f, x*0.66666f, 0.f));//środek 256
	//o->matM =  glm::scale(o->matM, glm::vec3(0.33333f, 0.33333f, 0.f));
	o->x = x;
	o->y = y;
	if(x<16) o->ekran=0;
	else o->ekran = round((x-16)/30)+1;
	o->bonus = 0;
	o->type =0;
	enemy[o->ekran].push_back(*o);
	
	
}
void addAmmo(vector<OBJ> &ammo,Matrix matrix,OBJ *prototype,int x, int y,bool side)
{
	OBJ *o = new OBJ();
	o->setupOBJ(prototype);
	o->setupTexAll(prototype);
	int left = side ? -1: 1;
	
	o->matM =  glm::translate(matrix.M_begin , glm::vec3(-(y+1)*0.66666f, (x+2.8f*left)*0.66666f, 0.f));//środek 256
	o->matM =  glm::scale(o->matM, glm::vec3(0.33333f, 0.33333f, 0.f));
	o->matM2 = o->matM;
	o->x2 = x;
	o->x = x + left;
	o->y = y;
	o->bonus = 1;
	o->lookAtLeft = side;
	ammo.push_back(*o);
}
void addColor(vector<vector<OBJ>>  &enemy,Matrix matrix,OBJ *prototype,int x, int y)
{
	OBJ *o = new OBJ();
	o->setupOBJ(prototype);
	o->setupTexAll(prototype);
	
	o->matM =  glm::translate(matrix.M_begin , glm::vec3(-y*0.66666f, x*0.66666f, 0.f));//środek 256
	//o->matM =  glm::scale(o->matM, glm::vec3(0.33333f, 0.33333f, 0.f));
	o->x = x;
	o->y = y;
	if(x<16) o->ekran=0;
	else o->ekran = round((x-16)/30)+1;
	o->bonus = 0;
	o->type =1;
	enemy[o->ekran].push_back(*o);
	
	
}
void addSklot(vector<vector<OBJ>>  &enemy,Matrix matrix,OBJ *prototype,int x, int y)
{
	
	
	OBJ *o = new OBJ();
	o->setupOBJ(prototype);
	o->setupTexAll(prototype);
	
	
	o->matM =  glm::translate(matrix.M_begin , glm::vec3(-(y+5)*0.66666f, x*0.66666f, 0.f));//środek 256
	o->matM =  glm::scale(o->matM, glm::vec3(7.7f, 7.7f, 0.f));
	o->matM = glm::rotate(o->matM ,-180.0f,glm::vec3(1,0,0));

	o->x = x;
	o->y = y;
	if(x<16) o->ekran=0;
	else o->ekran = round((x-16)/30)+1;
	o->bonus = 0;
	o->type =7;
	enemy[o->ekran].push_back(*o);
	
	
}
void addRainbow(vector<vector<OBJ>>  &enemy,Matrix matrix,OBJ *prototype,int x, int y)
{
	
	
	OBJ *o = new OBJ();
	o->setupOBJ(prototype);
	o->setupTexAll(prototype);
	
	
	o->matM =  glm::translate(matrix.M_begin , glm::vec3(-(y+3)*0.66666f, x*0.66666f, 0.f));//środek 256
	o->matM =  glm::scale(o->matM, glm::vec3(9.f, 9.f, 0.f));
	o->matM = glm::rotate(o->matM ,-180.0f,glm::vec3(1,0,0));
	
	o->x = x;
	o->y = y;
	if(x<16) o->ekran=0;
	else o->ekran = round((x-16)/30)+1;
	o->bonus = 0;
	o->type =3;
	enemy[o->ekran].push_back(*o);
	
	
}
void addTrash(vector<vector<OBJ>>  &enemy,Matrix matrix,OBJ *prototype,int x, int y)
{
	
	
	OBJ *o = new OBJ();
	o->setupOBJ(prototype);
	o->setupTexAll(prototype);
	
	
	o->matM =  glm::translate(matrix.M_begin , glm::vec3(-(y)*0.66666f, x*0.66666f, 0.f));//środek 256
	o->matM =  glm::scale(o->matM, glm::vec3(1.f, .666f, 0.f));
	o->matM = glm::rotate(o->matM ,-180.0f,glm::vec3(1,0,0));

	o->x = x;
	o->y = y;
	if(x<16) o->ekran=0;
	else o->ekran = round((x-16)/30)+1;
	o->bonus = 0;
	o->type =4;
	enemy[o->ekran].push_back(*o);
	
	
}
void addThomb(vector<vector<OBJ>>  &enemy,Matrix matrix,OBJ *prototype,int x, int y)
{
	
	
	OBJ *o = new OBJ();
	o->setupOBJ(prototype);
	o->setupTexAll(prototype);
	
	
	o->matM =  glm::translate(matrix.M_begin , glm::vec3(-(y+3)*0.66666f, x*0.66666f, 0.f));//środek 256
	o->matM =  glm::scale(o->matM, glm::vec3(1.f, .666f, 0.f));
	o->matM = glm::rotate(o->matM ,-180.0f,glm::vec3(1,0,0));
	o->matM =  glm::scale(o->matM, glm::vec3(3.f, 3.f, 0.f));
	o->x = x;
	o->y = y;
	if(x<16) o->ekran=0;
	else o->ekran = round((x-16)/30)+1;
	o->bonus = 0;
	o->type =8;
	enemy[o->ekran].push_back(*o);
	
	
}
void addBlock(vector<vector<OBJ>> &enemy,Matrix matrix,OBJ *prototype,int x, int y)
{
	OBJ *o = new OBJ();
	o->setupOBJ(prototype);
	o->setupTexAll(prototype);
	o->matM =  glm::translate(matrix.M_begin , glm::vec3(-y*0.66666f, x*0.66666f, 0.f));//środek 256
	o->matM =  glm::scale(o->matM, glm::vec3(0.33333f, 0.33333f, 0.f));
	o->x = x;
	o->y = y;
	if(x<16) o->ekran=0;
	else o->ekran = round((x-16)/30)+1;
	o->bonus = 0;
	o->type =0;
	//cout<<o->ekran<<endl;
	enemy[o->ekran].push_back(*o);
}
void addBrick(vector<vector<OBJ>> &enemy,Matrix matrix,OBJ *prototype,int x, int y)
{
	OBJ *o = new OBJ();
	o->setupOBJ(prototype);
	o->setupTexAll(prototype);
	o->matM =  glm::translate(matrix.M_begin , glm::vec3(-y*0.66666f, x*0.66666f, 0.f));//środek 256
	o->matM =  glm::scale(o->matM, glm::vec3(0.33333f, 0.33333f, 0.f));
	o->x = x;
	o->y = y;
	if(x<16) o->ekran=0;
	else o->ekran = round((x-16)/30)+1;
	o->bonus = 0;
	o->type =1;
	enemy[o->ekran].push_back(*o);
}
void addThunder(vector<vector<OBJ>> &enemy,Matrix matrix,OBJ *prototype,int x, int y)
{
	OBJ *o = new OBJ();
	o->setupOBJ(prototype);
	o->setupTexAll(prototype);
	o->matM =  glm::translate(matrix.M_begin , glm::vec3(-y*0.66666f, x*0.66666f, 0.f));//środek 256
	o->matM =  glm::scale(o->matM, glm::vec3(0.33333f, 0.33333f, 0.f));
	o->x =x;
	o->y =y;
	if(x<16) o->ekran=0;
	else o->ekran = round((x-16)/30)+1;
	o->bonus = 3;
	enemy[o->ekran].push_back(*o);
	//cout<<"dodano"<<endl;
}
void enemyLookAt(OBJ &o,int dist_min)
{
	

	bool lookAtLeft;
	if((o.type <2)&&(o.lookAtLeft) != (lookAtLeft = (dist_min>0) ? true: false))
	{
		o.lookAtLeft = lookAtLeft;
		glm::mat4 matR =glm::rotate( glm::mat4(1.0f) ,180.f,glm::vec3(1.f,0.f,0.f));
		o.matM = o.matM*matR;
	}
	
}
int enemyDistance(OBJ &o,vector<OBJ> postacie,int &dist_min,int distance,int &nr)
{
	dist_min=10000;	
	nr = -1;
	int flag =0;
	for(int i=0;i<postacie.size();i++)
	{
	
		//cout<<"p:"<<i<<" -> "<<postacie[i].x<<" 	e:"<<enemyId<<" -> "<<enemy[enemyId].x<<endl;
		int dist = o.x - postacie[i].x;
		if(abs(dist)<abs(dist_min)) dist_min = dist;

		if(abs(dist)<distance) 
		{
			flag= 1;
			if(nr==-1) nr=i;
			else nr = 100;
		}
	}
	return flag;
}
