#ifndef MAIN_H
#define MAIN_H
#include "GL/glew.h"
#include "GL/freeglut.h"
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtc/type_ptr.hpp"
#include <stdio.h>
#include <iostream>
#include <vector>
#include <string>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <linux/joystick.h>
#include <stdlib.h> 
#include <cstdlib>
#include <time.h>   
#include <ft2build.h>
#include FT_FREETYPE_H
#include "sound.h"
#include "tga.h"
#include "OBJ.h"
#include "player.h"
#include "mouse.h"
#include "shaderprogram.h"
#include "shader_utils.h"
#include "text.h"
#include "gra.h"
#include "display.h"
#include "postprocess.h"
#include "enemy.h"

#define JOY_DEV0 "/dev/input/js0"
#define JOY_DEV1 "/dev/input/js1"
#define spaceX 1.1f 
#define spaceY 2.0f
#define MOVE_A 0.0002f
#define SPACE_A 15.f
#define SPACE_MAX 90.f
#define MAPA "./other/mapa"
#define PLANE "./other/plane.obj"
#define FONT_NAME "./other/FreeSans.ttf"
#define FSHADER_POST "./shader/fshaderPost.txt"
#define VSHADER_POST "./shader/vshaderPost.txt"
#define FSHADER_POST_WAVE "./shader/fshaderPostWave.txt"
#define FSHADER_MAIN "./shader/fshader.txt"
#define VSHADER_MAIN "./shader/vshader.txt"
#define VSHADER_MOUSE "./shader/vshaderP.txt"
#define FSHADER_MOUSE "./shader/fshader.txt"
#define VSHADER_PLAYER "./shader/vshaderPR.txt"
#define FSHADER_PLAYER "./shader/fshader.txt"
#define VSHADER_TEXT "./shader/text.v.glsl"
#define FSHADER_TEXT "./shader/text.f.glsl"
#define MAP_X 5
#define MAP_Y 2
#define PINK_RANGE 13
#define AMMO_SPEED 3000 // speed = 1/AMMO_SPEEDs
#define BACKGROUND_PATH "./texture/background/"
#define MAX_POINT 2
#define BACKGOUND_NUMBER 25
#define MAX_ENEMY_COLOR 5
void initObiectMap();
int initPostProgram(const char* fFilename);
void mouseBack(bool back); 
void loadObject(OBJ *o1,string filename,string filename2,int x, int y,int bonus);
int mod(int i,int j);
int collisionX(int j);
#endif
