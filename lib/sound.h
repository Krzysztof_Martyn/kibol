#include "main.h"
#include <stdio.h>
#include <AL/al.h>
#include <AL/alc.h>
//#include <AL/alu.h>
#include <AL/alut.h>

#define NUM_BUFFERS 18
#define NUM_SOURCES 18
#define NUM_ENVIRONMENTS 1

#define MAX_SOUND 18

#ifndef SOUND_H
#define SOUND_H
void startNewSound(int i, int &sound_nr);
void stopSound(int &sound_nr);
void addSound(int i, const char * name);
void initSound(void);
void soundEnd();
void soundStart(int i);
void soundStop(int i);

#endif
